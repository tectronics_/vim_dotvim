This is the personal terminal configuration setup guide  that I use for working with Vim to develop in Ruby on Rails, Javascript, etc.
Feel free to try it out. I am always adding vim plugins as I continue to learn vim. I have also added [XVim](https://github.com/XVimProject/XVim) to my XCode configuration.

Thanks.


###Install Instructions###
---

Clone vim_dotvim directory to your home directory.

Rename downloaded directory:

__```mv ~/vim_dotvim ~/.vim```__

Symlink the .vimrc file to your home directory using:

__```ln -s ~/.vim/.vimrc .vimrc```__

This vim setup supports plugins using Vundle for plugin management.

Install [Vundle](https://github.com/gmarik/Vundle.vim)

__```git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim```__

Open vim (type vim in you terminal) and enter 

__```:PluginInstall```__

from within vim to install / update plugins.

###Plugins supported include:###
---

- [Fugitive](https://github.com/tpope/vim-fugitive) (requires [git](http://git-scm.com/))
- [vim-git](https://github.com/tpope/vim-git)  (requires [git](http://git-scm.com/))
- [emmet-vim](https://github.com/mattn/emmet-vim)
- [vim-better-whitespace](https://github.com/ntpeters/vim-better-whitespace)
- [taglist](https://github.com/vim-scripts/taglist.vim) (requires [CTags](http://andrew.stwrt.ca/posts/vim-ctags))
- [mru](https://github.com/yegappan/mru)
- [vim-javascript-syntax](https://github.com/jelera/vim-javascript-syntax)
- [vim-rails](https://github.com/tpope/vim-rails)
- [syntastic](https://github.com/scrooloose/syntastic)
- [nerdtree](https://github.com/scrooloose/nerdtree)
- [nerdcommenter](https://github.com/scrooloose/nerdcommenter)
- [vim-airline](https://github.com/bling/vim-airline)
- [supertab](https://github.com/ervandew/supertab)
- [auto-pairs](https://github.com/jiangmiao/auto-pairs)
- [surround](https://github.com/tpope/vim-surround)
- [ctrlp](https://github.com/kien/ctrlp.vim)
- [vim-startify](https://github.com/mhinz/vim-startify)

Note: If you do not have git or CTags installed, you can comment out any plugins that require these in your .vimrc file.

Vim uses " for commenting.

---
I also use Tmux, iTerm2, and oh-my-zsh

To set up [Tmux](http://tmux.sourceforge.net/)

To set up [iTerm2](http://iterm2.com/)

To set up [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)

My zsh theme is updated in ~/.zshrc

```ZSH_THEME="nanotech"```