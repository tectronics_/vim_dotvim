" ---------------------------------------------------
"  Vundle Plugins
" ---------------------------------------------------

" required vundle
set nocompatible
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install
" plugins
" call vundle#begin('~/some/path/here')


" Vundle manages vundle plugin
" required!
Plugin 'gmarik/vundle'


" ---------------------------------------------------
" My Plugins
" ---------------------------------------------------
" startup screen with better options
Plugin 'mhinz/vim-startify'
" emmet html shortcuts
Plugin 'mattn/emmet-vim'
" show/remove trailing whitespace
Plugin 'ntpeters/vim-better-whitespace'
" tag list right
Plugin 'taglist.vim'
" most recently used list
Plugin 'mru.vim'
" javascript specific
Plugin 'jelera/vim-javascript-syntax'
" rails specific
Plugin 'tpope/vim-rails.git'
" highlight syntax errors
Plugin 'scrooloose/syntastic'
" file navigation
Plugin 'scrooloose/nerdtree'
" shortcuts for commenting code
Plugin 'scrooloose/nerdcommenter'
" buffer bar top emulates tabs
Plugin 'bling/vim-airline'
" tab completions
Plugin 'ervandew/supertab'
" autopair
Plugin 'jiangmiao/auto-pairs'
" surround
Plugin 'tpope/vim-surround'
" git
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-git'
" Fuzzy finder (CTRL + P)
Plugin 'kien/ctrlp.vim'

" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on     " required!

" Brief help
" :PluginList          - list configured bundles
" :PluginInstall(!)    - install (update) bundles
" :PluginSearch(!) foo - search (or refresh cache first) for foo
" :PluginClean(!)      - confirm (or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Plugin commands are not allowed.

syntax on
" ---------------------------------------------------
" Leader Mapped to cd (change directory) used to move
" between buffers and to see buffer list or make new
" buffer - uses airline plugin for buffer management
" ---------------------------------------------------

" set leader to space
let mapleader=' '

" Working with Buffers Instead of Tabs
" See: https://joshldavis.com/2014/04/05/vim-tab-madness-buffers-vs-tabs/"

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" This allows buffers to be hidden if you've modified a buffer.
" This is almost a must if you wish to use buffers in this way.
set hidden

" To open a new empty buffer
" This replaces :tabnew for buffers not tabs
nmap <leader>T :enew<cr>

" Move to the next buffer
nmap <leader>l :bnext<CR>

" Move to the previous buffer
nmap <leader>h :bprevious<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>

" Close the current buffer without saving and move to
" the previous one
nmap <leader>bQ :bd!

" Show all open buffers and their status
nmap <leader>bl :ls<CR>

" ---------------------------------------------------
"  Nerd Tree F2
" ---------------------------------------------------

nmap <F2> :NERDTreeToggle<cr>
vmap <F2> <esc>:NERDTreeToggle<cr>i
imap <F2> <esc>:NERDTreeToggle<cr>i

" closes NERDTree when no file is open so that :q will just close vim
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" opens NERDTree even if no file has been selected
" autocmd vimenter * if !argc() | NERDTree | endif

" ----------------------------------------------------
" Tags List F3
" ----------------------------------------------------

" Tags list right
nnoremap <F3> :TlistToggle<CR>
let Tlist_Use_Right_Window   = 1

" ----------------------------------------------------
" Autoformat F4
" ----------------------------------------------------

" sets :Autoformat command as F4
noremap <F4> :Autoformat<CR><CR>

" ----------------------------------------------------
" Emmet Contol y leader
" ----------------------------------------------------

" emmet leader key is control y
let g:user_emmet_leader_key = '<C-y>'

" ----------------------------------------------------
" Color Theme
" ----------------------------------------------------

colorscheme distinguished

" ----------------------------------------------------
" Control p ignore files and show hidden
" ----------------------------------------------------

" ignored files for ctrlp and MRU
" MacOSX/Linux
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.jpg,*.png,*.tif,*.psd
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'

" invisible files show in ctrlp
let g:ctrlp_show_hidden = 1

" ----------------------------------------------------
" Map escape to ;; for insert and vv for visual
" ----------------------------------------------------

" ----------------------------------------------------
"  Now using Control [ instead of escape
"  Previous use of ii and vv was being delayed in
"  intellij and xcode vim simulations
"  I have set my caps lock key to be another control key
"  which makes it much easer to leave insert mode this way
" ----------------------------------------------------
" set escape from insert mode to ii"
" inoremap ii <Esc> LEGACY
" vnoremap vv <Esc> LEGACY

" ----------------------------------------------------
" add line above and below from normal
" ----------------------------------------------------

" add line above
nmap [<space> O<Esc>j

" add line below
nmap <CR> o<Esc>k

" ----------------------------------------------------
" Formatting
" ----------------------------------------------------

" fixes problem with escape key delay when exiting visual
set timeoutlen=1000 ttimeoutlen=0

" indent automatically based upon that of the last line
set autoindent

" smarter indenting for brackets, etc
set smartindent

" linebreak
set wrap linebreak nolist

" add a vertical column guide at column 90
set colorcolumn=90

" always show this number of lines above/below 
set scrolloff=5

" set line numbers to on
set number

" set indent spacing
set shiftwidth=2
set tabstop=2
set softtabstop=2
set expandtab

" html does not work in syntastic so you can run it with :SyntasticCheck
let syntastic_mode_map = { 'passive_filetypes': ['html']  }

" ----------------------------------------------------
" Cursor Formatting
" ----------------------------------------------------

" change line hightlight and column highlight
:hi CursorLine cterm=NONE ctermbg=236 

" cursor line highlight is only applied to current window
augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline
augroup END

" Show syntax highlighting groups for word under cursor
nmap <C-S-P> :call <SID>SynStack()<CR>

" ----------------------------------------------------
" Javadoc style comments
" ----------------------------------------------------

" add javadoc style comment
" if /* typed next line will act like another comment line
iab <buffer> /// /**^M *^M*/^[0A

" line breaks within comment are auto commented
set formatoptions=c,q,r,t

" ----------------------------------------------------
" Format based on file extensions
" ----------------------------------------------------

" automatic formatting based on extensions
autocmd BufWritePre *.rb :%s/\s\+$//e
autocmd BufWritePre *.html :%s/\s\+$//e
autocmd BufWritePre *.scss :%s/\s\+$//e
autocmd BufWritePre *.jsx :%s/\s\+$//e
autocmd BufWritePre *.js :%s/\s\+$//e
au BufRead,BufNewFile *.php set ft=php.html

" ----------------------------------------------------
" Mouse support
" ----------------------------------------------------

" mouse support for resizing panes
" Send more characters for redraws
set ttyfast

" Enable mouse use in all modes
set mouse=a

" Set this to the name of your terminal that supports mouse codes.
" Must be one of: xterm, xterm2, netterm, dec, jsbterm, pterm
set ttymouse=xterm2

" change cursor for differnt modes - block cursor when not in insert mode
if exists('$TMUX')
  let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
  let &t_SI = "\<Esc>]50;CursorShape=1\x7"
  let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif
:autocmd InsertEnter * set cul
:autocmd InsertLeave * set nocul


" ----------------------------------------------------
" Source vimrc on write
" ----------------------------------------------------

" source vimrc on save
autocmd bufwritepost .vimrc source $MYVIMRC
